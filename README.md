# Hide and Seek

## Description

Hide-and-seek game server that handles maze generation and custom AI creation.

---
## Get the server part here: [persimankita/hide-seek-client](https://gitlab.com/persimankita/hide-seek-client)

## Installation

Create project executables
```
mkdir build
cd build
cmake ..
make -j
```

## Run program


From `build` folder, you can run the server:
```bash
./src/server
```

**Note:** by default port is 8080

And also a Client (Hider or Seeker):
```
./src/client 8081 0 random
```

Currently, 4 parameters:

- 1°) the client PORT
- 2°) Seeker or Hider client choice: `0` for Seeker, `1` for Hider
- 3°) Choice of IA (if multiple have been registered)
- 4°) Choice of Maze (only for Hider) 

**Note:** your IA need to inherit from `src/player/hider.hpp` or `src/player/seeker.hpp` and be registered in the main of `src/client.cpp`.

Example:
```cpp
// register the RandomHider class as "random"
HiderFactory::registerClass<RandomHider>("random");
```

## Run test

From `build` folder:
```
./src/test/test_hide_and_seek
```


## Others indications

**Note:** you can also use `DCMAKE_BUILD_TYPE=Release` for random seed and better performances.

Run the server from build folder:
```bash
./src/server
```
