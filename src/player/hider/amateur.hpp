//
// Created by persi on 30/05/22.
//

#ifndef HIDE_AMATEUR_HPP
#define HIDE_AMATEUR_HPP

#include "hider.hpp"
#include "../seeker/seeker.hpp"


using namespace std;

class AmateurHider:public Hider{
public:
    explicit AmateurHider(const string&  name);
    virtual const PlayerAction & play(const shared_ptr<Observation>&observation,const vector<PlayerAction>&actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);
    ~AmateurHider();

};
#endif //HIDE_AMATEUR_HPP
