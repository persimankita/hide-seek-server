//
// Created by persi on 30/05/22.
//

#include "amateur.hpp"

/**
 * @brief Construct a new ExpertHider: ExpertHider object
 * @param name
 */

AmateurHider::AmateurHider(const string &name): Hider(name){}


const PlayerAction &
AmateurHider::play(const shared_ptr<Observation> &observation,  const vector<PlayerAction> &actions) const{
    std::vector<std::shared_ptr<MazeCell>> obs = observation->getCells();
    
    //for each cell of observation
    for (shared_ptr<MazeCell> val:obs) {
        //cell contain player that have not the same role
        if (val->hasPlayer() && this->getRole() != val->getPlayer()->getRole()){
            //remember location
            Point me = this->getLocation();
            Point seek=val->getPlayer()->getLocation();
            //Find action in the vector
            auto it = std::find_if(actions.begin(),actions.end(),[&me,&seek] (const auto  &result){
                if (me.x < seek.x) {
                    return result.orientation != RIGHT;
                }
                if (me.x > seek.x) {
                    return result.orientation != LEFT;
                }
                if (me.y > seek.y) {
                    return result.orientation != TOP;
                }
                if (me.y < seek.y) {
                    return result.orientation != BOTTOM ;
                }
                return false;
            });
            //return action
            if (it != actions.end()){
                return (*it);
            }

        }
    }
    

    return actions[RandGenerator::getInstance()->getRandomInt(actions.size() - 1)];

}


void AmateurHider::update(const shared_ptr<Observation> &prev, const shared_ptr<Observation> &next,
                         const PlayerAction &action) {
    Hider::update(prev, next, action);
}

/**
 *
 * @brief Destroy the Hider: Hider object
 */
AmateurHider::~AmateurHider() {}
