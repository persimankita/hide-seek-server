#include "player/hider/random.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>
#include <unistd.h>

/**
 * @brief Construct a new RandomHider:: RandomHider object
 * 
 * 
 * @param name 
 */
RandomHider::RandomHider(const std::string &name) : Hider(name){}


/**
 * @brief Destroy the Hider:: Hider object
 * 
 */
RandomHider::~RandomHider() {}

/**
 * @brief Return a random action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &RandomHider::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {
    
    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
    

    return actions[rGenerator->getRandomInt(actions.size() - 1)]; 
}

void RandomHider::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {
    // Do something...
};