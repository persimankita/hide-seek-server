#ifndef HIDE_AND_SEEK_ACTION_HPP
#define HIDE_AND_SEEK_ACTION_HPP

#include <string>
#include <vector>
#include <utils/json.hpp>

/**
 * @brief Action enum which stores all possible action of a player
 * 
 * Seeker can interact with block in order to reduce its resistance and pass to find hider
 * Hider can carry and place block in order to protect itself
 * Hider and seeker can also do nothing
 */
enum Interaction {
    MOVE, 
    HIT,
    CARRY,
    PLACE,
    NOTHING
};

enum Orientation {
    TOP, BOTTOM, LEFT, RIGHT
};

/**
 * @brief Store the Player Action
 * 
 */
class PlayerAction {

    public:
        Orientation orientation;
        Interaction interaction;

        PlayerAction() {}
        
        PlayerAction(Orientation orientation, Interaction interaction) 
            : orientation(orientation), interaction(interaction) {}

        nlohmann::json toJSON() const;
        static PlayerAction fromJSON(const nlohmann::json &data);

        static const std::vector<Orientation> getOrientations() {
            return {Orientation::TOP, Orientation::BOTTOM, Orientation::LEFT, Orientation::RIGHT};
        }
};

std::string interactionToString(enum Interaction interaction);
std::string orientationToString(enum Orientation orientation);
std::string actionToString(const PlayerAction &action);

#endif