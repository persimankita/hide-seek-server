#include "player/action.hpp"

std::string interactionToString(enum Interaction interaction) {
    switch (interaction)
    {
    case MOVE:
        return "Move";
        break;
    case HIT:
        return "Hit";
        break;
    case CARRY:
        return "Carry";
        break;
    case PLACE:
        return "Place";
        break;
    case NOTHING:
        return "Nothing";
        break;
    default:
        return "Unknown";
        break;
    }
}

std::string orientationToString(enum Orientation orientation) {
    switch (orientation)
    {
    case TOP:
        return "Top";
        break;
    case BOTTOM:
        return "Bottom";
        break;
    case RIGHT:
        return "Right";
        break;
    case LEFT:
        return "Left";
        break;
    default:
        return "Unknown";
        break;
    }
}

nlohmann::json PlayerAction::toJSON() const {

    nlohmann::json json_data;

    json_data["orientation"] = orientation;
    json_data["interaction"] = interaction;

    return json_data;
}

PlayerAction PlayerAction::fromJSON(const nlohmann::json &data) {

    auto orientation = data["orientation"].get<Orientation>();
    auto interaction = data["interaction"].get<Interaction>();
    
    return PlayerAction(orientation, interaction);
}

/**
 * @brief Retrieve the string representation of the action
 * 
 * @param action 
 * @return std::string 
 */
std::string actionToString(const PlayerAction &action) {
    return std::string("(" + orientationToString(action.orientation)
            + ", " +  interactionToString(action.interaction) + ")");
}