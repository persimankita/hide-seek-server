#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_HPP

#include <string>
#include <vector>
#include <memory>
#include <utils/json.hpp>

#include "player/player.hpp"


/**
 * @brief Abstract Seeker class
 * 
 */
class Seeker : public Player
{
public:
    Seeker(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {
        // by default...
        return actions.at(0);
    };

    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {};
    virtual Role getRole() const;
    
    ~Seeker();
};

#endif