//
// Created by persi on 31/05/22.
//

#ifndef SEEK_AMATEUR_HPP
#define SEEK_AMATEUR_HPP
#include "seeker.hpp"
#include "../hider/hider.hpp"


using namespace std;

class AmateurSeeker:public Seeker{
public:
    AmateurSeeker(const string  name);
    AmateurSeeker() = default;
    virtual const PlayerAction & play(const shared_ptr<Observation>&item,const vector<PlayerAction>&actions) const;
    ~AmateurSeeker();

};
#endif //SEEK_AMATEUR_HPP
