//
// Created by persi on 31/05/22.
//

#include "amateur.hpp"

AmateurSeeker::AmateurSeeker(const string name) : Seeker(name) {

}

const PlayerAction &AmateurSeeker::play(const shared_ptr<Observation> &observation, const vector<PlayerAction> &actions) const{
    std::vector<std::shared_ptr<MazeCell>> obs = observation->getCells();
    //for each cell of observation
    for (shared_ptr<MazeCell> val:obs) {
        //cell contain player that have not the same role
        if (val->hasPlayer() && this->getRole() != val->getPlayer()->getRole()){
            //remember location
            Point me = this->getLocation();
            Point hide=val->getPlayer()->getLocation();
            //Find action in the vector
            auto it = std::find_if(actions.begin(),actions.end(),[&me,&hide] (const auto  &result){
                if (me.x < hide.x) {
                    return (result.orientation == RIGHT && result.interaction == MOVE);
                }
                if (me.x > hide.x) {
                    return (result.orientation == LEFT && result.interaction == MOVE);
                }

                if (me.y > hide.y) {
                    return (result.orientation == TOP && result.interaction == MOVE) ;
                }
                if (me.y < hide.y) {
                    return (result.orientation == BOTTOM && result.interaction == MOVE);
                }
                return false;
            });
            //return action


            if(it != actions.end()) {
                return (*it);
            }

        }
    }
    return actions[RandGenerator::getInstance()->getRandomInt(actions.size() - 1)];
}

AmateurSeeker::~AmateurSeeker() {

}
