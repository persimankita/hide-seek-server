#include "playerFactory.hpp"

template<> PlayerFactory<Hider>::MapType PlayerFactory<Hider>::types {};
template<> PlayerFactory<Seeker>::MapType PlayerFactory<Seeker>::types {};
template<> PlayerFactory<Hider>::MapFrom PlayerFactory<Hider>::origins {};
template<> PlayerFactory<Seeker>::MapFrom PlayerFactory<Seeker>::origins {};