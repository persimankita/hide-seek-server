#ifndef HIDE_AND_SEEK_PLAYER_FACTORY_HPP
#define HIDE_AND_SEEK_PLAYER_FACTORY_HPP

#include <memory>
#include <string>
#include <vector>
#include <type_traits>

#include "player/seeker/seeker.hpp"
#include "player/hider/hider.hpp"

#include "utils/exceptions.hpp"

/**
 * @brief Factory for player which create Hider or Seeker based on class name
 * Need to register class
 * 
 */
template<typename Base>
class PlayerFactory
{
public:
    using Instantiate = std::function<std::shared_ptr<Base>(const std::string &)>;
    using MapType = std::map<std::string, Instantiate>;
        
    using From = std::function<std::shared_ptr<Base>(const nlohmann::json &)>;
    using MapFrom = std::map<std::string, From>;
 
    static std::shared_ptr<Base> create(std::string const& className, std::string const& name) {

        // try to find the registered function
        auto it = types.find(className);
        if(it == types.end())
            return nullptr;
        return it->second(name);
    }

    static std::shared_ptr<Base> fromJSON(std::string const& className, const nlohmann::json &data) {

        // try to find the registered function
        auto it = origins.find(className);
        if(it == origins.end())
            return nullptr;
        return it->second(data);
    }

    static bool isRegistered(const std::string &className) {
        
        for(auto it = types.begin(); it != types.end(); ++it) {
            if (it->first == className)
                return true;
        }
        return false;
    }

    protected:
        // register create method for hiders and seekers
        static MapType types;
        static MapFrom origins;

        // template method in order to create Hider or Seeker
        template<typename T>
        static std::enable_if_t<std::is_base_of<Base, T>::value, std::shared_ptr<T>>
        create(const std::string &name) {
            
            return std::make_shared<T>(name);
        }
};

class SeekerFactory : public PlayerFactory<Seeker> {

    public:
        template<typename T>
        static std::enable_if_t<std::is_base_of<Seeker, T>::value, void>
        registerClass(const std::string &className) {
            
            // replace by default the key...
            types[className] = &SeekerFactory::create<T>;
            origins[className] = &SeekerFactory::fromJSON<T>;
        }

    private:
        template<class T>
        static std::enable_if_t<std::is_base_of<Seeker, T>::value, std::shared_ptr<T>>
        fromJSON(const nlohmann::json &data)
        {
            std::shared_ptr<T> seeker = std::make_shared<T>(data["name"].get<std::string>());

            seeker->setOrientation(data["orientation"].get<Orientation>());

            auto x = data["location"]["x"].get<int>();
            auto y = data["location"]["y"].get<int>();
            seeker->setLocation(x, y);

            return seeker;
        }

};

class HiderFactory : public PlayerFactory<Hider> {
    
    public:
        template<typename T>
        static std::enable_if_t<std::is_base_of<Hider, T>::value, void>
        registerClass(const std::string &className) {
            
            // replace by default the key...
            types[className] = &HiderFactory::create<T>;
            origins[className] = &HiderFactory::fromJSON<T>;
        }

    private:

        template<class T>
        static std::enable_if_t<std::is_base_of<Hider, T>::value, std::shared_ptr<T>>
        fromJSON(const nlohmann::json &data)
        {
            std::shared_ptr<T> hider = std::make_shared<T>(data["name"].get<std::string>());

            hider->setOrientation(data["orientation"].get<Orientation>());
            hider->setCaptured(data["captured"].get<bool>());

            auto x = data["location"]["x"].get<int>();
            auto y = data["location"]["y"].get<int>();
            hider->setLocation(x, y);

            if (data.contains("block")) {
                hider->setBlock(Block::fromJSON(data["block"]));
            }

            return hider;
        }
};

template<> PlayerFactory<Hider>::MapType PlayerFactory<Hider>::types;
template<> PlayerFactory<Seeker>::MapType PlayerFactory<Seeker>::types;
template<> PlayerFactory<Hider>::MapFrom PlayerFactory<Hider>::origins;
template<> PlayerFactory<Seeker>::MapFrom PlayerFactory<Seeker>::origins;

// expected to avoid multiple definitions of static members
extern template PlayerFactory<Hider>::MapType PlayerFactory<Hider>::types;
extern template PlayerFactory<Seeker>::MapType PlayerFactory<Seeker>::types;
extern template PlayerFactory<Hider>::MapFrom PlayerFactory<Hider>::origins;
extern template PlayerFactory<Seeker>::MapFrom PlayerFactory<Seeker>::origins;

#endif