#include "rest_client.hpp"
#include "factory/playerFactory.hpp"
#include "factory/mazeFactory.hpp"

// specific incluces
#include "maze/custom/basic.hpp"
#include "player/seeker/random.hpp"
#include "player/hider/random.hpp"
#include "maze/custom/intermediate.hpp"
#include "player/seeker/amateur.hpp"
#include "player/hider/amateur.hpp"

struct sigaction old_action;

// default params
namespace Params {

    int serverPort = 8080;
    std::string serverAddress = "0.0.0.0";

    int clientPort = 8081;
    std::string clientAddress = "0.0.0.0";
    std::string className = "random";
    Role role = Role::HIDER;
};

int main(int argc, char* argv[]) {

    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<IntermediateMaze>("intermediate");
    SeekerFactory::registerClass<AmateurSeeker>("amateur");
    HiderFactory::registerClass<AmateurHider>("amateur");

    std::string mazeClassName = "intermediate"; // by default

    if (argc >= 2) {
        // GET PORT
        Params::clientPort = static_cast<uint16_t>(std::stol(argv[1]));
    }

    if (argc >= 3) {
        // GET ROLE
        int roleValue = static_cast<uint16_t>(std::stol(argv[2]));

        if (roleValue == 0)
            Params::role = Role::SEEKER;
        else if (roleValue == 1) 
            Params::role = Role::HIDER;
        else {
            std::cout << "Invalid Role argument" << std::endl;
            return 0;
        }
    }

    if (argc >= 4) {
        // GET ClassName
        Params::className = argv[3];

        if (Params::role == Role::HIDER) {
            if (!HiderFactory::isRegistered(Params::className)) {
                std::cout << "Invalid " << roleToString(Params::role) << " class name" << std::endl;
                return 0;   
            }
            else if (argc >= 5) {
                // GET Hider Maze 
                mazeClassName = argv[4];

                if (!MazeFactory::isRegistered(mazeClassName)) {
                    std::cout << "Invalid " << mazeClassName << " class name for Maze" << std::endl;
                    return 0;   
                }
            }
        }
        else if (Params::role == Role::SEEKER) {
            if (!SeekerFactory::isRegistered(Params::className)) {
                std::cout << "Invalid " << roleToString(Params::role) << " class name" << std::endl;
                return 0;   
            }
        }
    }

    Client client(Params::clientAddress, Params::clientPort, Params::role, Params::className);

    if (Params::role == Role::HIDER) {
        client.setMazeClass(mazeClassName);
    }

    client.init();
    client.connect(Params::serverAddress, Params::serverPort, Params::role);


    // Catch end of program
    struct Handler {
        static void disconnect(int sig_no) {

            httplib::Client cli(Params::serverAddress, Params::serverPort);

            nlohmann::json json_data;

            // send current client data information
            json_data["port"] = Params::clientPort;
            json_data["address"] = Params::clientAddress;
            json_data["role"] = Params::role;
            json_data["className"] = Params::className;

            std::cout << "Data sent: " << json_data << std::endl;

            if (auto res = cli.Post("/disconnect", json_data.dump(), "application/json")) {

                if (res->status == 200) {
                    std::cout << res->body << std::endl;
                } else {
                    std::cout << "Error while disconnecting from server..." << std::endl;
                }
            } else {
                std::cout << res.error() << std::endl;
                std::cout << "Error while disconnecting from server..." << std::endl;
                exit(0);
            }

            sigaction(SIGINT, &old_action, NULL);
            kill(0, SIGINT);
        }
    };

    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = &Handler::disconnect;
    sigaction(SIGINT, &action, &old_action);
    sigaction(SIGKILL, &action, &old_action);
    sigaction(SIGTERM, &action, &old_action);
    sigaction(SIGABRT, &action, &old_action);

    client.start();
}