#include "game.hpp"

#include <algorithm>
#include <list>

#include "maze/observation.hpp"
#include "player/hider/hider.hpp"
#include "player/seeker/seeker.hpp"

ClientPlayer::ClientPlayer(int port, const std::string address, const std::string className, Role role)
    : port(port), address(address), className(className), role(role) {}

bool ClientPlayer::addPlayer(const std::shared_ptr<Player> &player) {

    if (player->getRole() == role) {
        players.push_back(player);
        return true;
    }
    else {
        return false;
    }
}

std::string ClientPlayer::getURL() {
    return address + ":" + std::to_string(port);
}

std::string ClientPlayer::getAddress() const {
    return address;
}

int ClientPlayer::getPort() const {
    return port;
}

Role ClientPlayer::getRole() const {
    return role;
}

const std::string ClientPlayer::getClassName() const {
    return className;
}

const std::vector<std::shared_ptr<Player>> &ClientPlayer::getPlayers(){
    return players;
}

Game::Game(const std::shared_ptr<Maze> &maze, const unsigned int &maxRounds, const std::shared_ptr<ClientPlayer> &hider, const std::shared_ptr<ClientPlayer> &seeker) 
    : maxRounds(maxRounds), nround(0), maze(maze), hiderClient(hider), seekerClient(seeker)
{
}

Game::Game(const std::shared_ptr<Maze> &maze, const unsigned int &maxRounds) 
    : maxRounds(maxRounds), nround(0), maze(maze)
{
}

/**
 * @brief Add new player into the Maze randomly
 * 
 * @param player 
 */
void Game::addPlayer(const std::shared_ptr<Player> &player) {
    this->players.push_back(player);

    // track the first player
    if (currentPlayer == nullptr) {
        currentPlayer = player;
    }

    // add player into maze
    maze->addPlayer(player);
}

const std::pair<std::shared_ptr<Observation>, std::vector<PlayerAction>> Game::getState(const std::shared_ptr<Player> &player) const {
    
    auto observation = maze->getObservationOfPlayer(player);
    std::vector<PlayerAction> actions = maze->getActionsOfPlayer(player);

    return std::make_pair(observation, actions);
}

/**
 * @brief Compute the next player move/action into the Maze and returned new state
 * 
 * @return const std::shared_ptr<Observation> 
 */
const std::shared_ptr<Observation> Game::step(const std::shared_ptr<Player> &player, const PlayerAction &action) {

    // do the player chosen action 
    auto newObservation = maze->doPlayerAction(player, action);

    // go to next player
    this->getNextPlayer();

    // return the new observation state and reward
    return newObservation;
}

/**
 * @brief Check if there is at least one hider not captured
 *        or max number of rounds reached
 * 
 * @return true 
 * @return false 
 */
const bool Game::end() const {

    // if number of rounds reached
    if (nround >= maxRounds) {
        return true;
    }

    // if there is at least one hider not captured, then it's not the end!
    for (auto p : players) {
        if (p->getRole() == Role::HIDER) {

            auto hider = std::dynamic_pointer_cast<Hider>(p);
            if (!hider->isCaptured())
                return false;
        }
    }

    return true;
}

/**
 * @brief Get the current winner of the game
 * 
 * @return const Role 
 */
Role Game::getCurrentWinner() const {
    for (auto p : players) {
        
        if (p->getRole() == Role::HIDER) {

            auto hider = std::dynamic_pointer_cast<Hider>(p);
            if (!hider->isCaptured())
                return Role::HIDER;
        }
    }

    return Role::SEEKER;
}

/**
 * @brief Return the next player which has to play
 * 
 * @return const std::shared_ptr<Player> 
 */
const std::shared_ptr<Player>  Game::getNextPlayer() { 

    // get filtered list
    std::vector<std::shared_ptr<Player>> availablePlayers;
    std::for_each(players.begin(), players.end(), [&availablePlayers] (const auto p)
    {
        // Check if hider is always in game
        if (p->getRole() == Role::HIDER) {
            auto hider = std::dynamic_pointer_cast<Hider>(p);

            if(!hider->isCaptured())
                availablePlayers.push_back(p);
        }
        else {
            availablePlayers.push_back(p);
        }
    });

    // basic end check (no more player..)
    if (this->end()) {
        currentPlayer = nullptr;
    } 
    else {

        auto it = std::find(availablePlayers.begin(), availablePlayers.end(), currentPlayer);

        // return the next element from current Player if exists
        if (++it != availablePlayers.end()) {
            currentPlayer = (*it);
        } 
        else 
        {
            currentPlayer = availablePlayers.at(0);

            // also increase the number of round (all available players have played)
            this->nround += 1; 
        }
    }

    return currentPlayer;   
}

/**
 * @brief JSON representation of game state
 * 
 * @return const nlohmann::json 
 */
const nlohmann::json Game::toJSON() const {

    nlohmann::json jsonGame = maze->toJSON();

    /*if (currentPlayer != nullptr) {
        jsonGame["currentClient"] = currentPlayer->toJSON();
        
        // retrieve actions
        std::list<nlohmann::json> actions;

        for (auto a : maze->getActionsOfPlayer(currentPlayer)) {
            nlohmann::json action = a.toJSON();
            actions.push_back(action);
        }

        jsonGame["currentClient"]["possibleActions"] = actions;
        
        // get observation
        auto obs = maze->getObservationOfPlayer(currentPlayer);
        jsonGame["currentClient"]["observation"] = obs->toJSON();
    }*/
    
    jsonGame["end"] = end();
    jsonGame["round"] = nround;
    jsonGame["maxRounds"] = maxRounds;
    jsonGame["hider"] = hiderClient->getClassName();
    jsonGame["seeker"] = seekerClient->getClassName();

    nlohmann::json playersJson;

    for (auto p : players) {
        playersJson.push_back(p->toJSON());
    }

    jsonGame["players"] = playersJson;

    return jsonGame;
}

const std::shared_ptr<Player> &Game::getCurrentPlayer() const {
    return currentPlayer;
}

const std::vector<std::shared_ptr<Player>> Game::getPlayers(Role role) const {
    
    std::vector<std::shared_ptr<Player>> rolePlayers;

    for (auto &p : players)
        if (p->getRole() == role)
            rolePlayers.push_back(p);

    return rolePlayers;
}

/**
 * @brief Give the number of rounds
 * 
 * @return const unsigned int& 
 */
const unsigned int &Game::getNumberOfRounds() const {
    return nround;
}

const unsigned int Game::getNumberOfPlayers() const {
    return players.size();
}

const std::shared_ptr<ClientPlayer> &Game::getClient(Role role) const {
    return role == Role::HIDER ? hiderClient : seekerClient;
}

const std::shared_ptr<Maze> &Game::getMaze() const {
    return maze;
}

Game::~Game()
{
}