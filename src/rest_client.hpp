#ifndef HIDE_AND_SEEK_REST_CLIENT_HPP
#define HIDE_AND_SEEK_REST_CLIENT_HPP

#include <utils/httplib.hpp>
#include <utils/json.hpp>
#include <cstdlib>

#include "player/action.hpp"
#include "factory/playerFactory.hpp"
#include "factory/mazeFactory.hpp"
#include "player/seeker/random.hpp"
#include "player/hider/random.hpp"

class Client
{
public:
    Client(std::string address, int port, Role role, const std::string &className) 
        : address(address), port(port), role(role), className(className) {}

    void init() {

        // default register
        HiderFactory::registerClass<Hider>("hider");
        SeekerFactory::registerClass<Seeker>("seeker");
        MazeFactory::registerClass<Maze>("maze");

        setupRoutes();
    }

    void setMazeClass(const std::string &className) {
        mazeClassName = className;
    }

    void connect(const std::string &serverAddress, int serverPort, Role role)
    {   
        try {
            httplib::Client cli(serverAddress, serverPort);

            nlohmann::json json_data;

            // send current client data
            json_data["port"] = port;
            json_data["address"] = address;
            json_data["role"] = role;
            json_data["className"] = className;

            if (auto res = cli.Post("/connect", json_data.dump(), "application/json")) {
                if (res->status == 200) {
                    std::cout << res->body << std::endl;
                } 
                else {
                    std::cout << "Error while connecting to server: " << res->body << std::endl;
                    exit(0);
                }
            } 
            else {
                std::cout << "Game server [" << serverAddress << ":" << serverPort << "] cannot be reached..." << std::endl;
                exit(0);
            }
        }
        catch(std::exception &e){
            std::cout << "Server not accessible" << std::endl;
            std::cout << e.what() << std::endl;
            exit(0);
        }
    }

    void start()
    {   
        std::cout << "[" << roleToString(role) << ": " << className << "] Client listen on: " << address << ":" << port << std::endl;
        std::cout << "[" << roleToString(role) << ": " << className << "] Client is waiting instructions..." << std::endl;
        svr.listen(address.c_str(), port);
    }

private:

    void setupRoutes()
    {
        svr.Get("/", [this](const httplib::Request &, httplib::Response &res) {
            res.set_content("This is a " + roleToString(role) + " client", "text/plain");
        });

        svr.Post("/game", [this](const httplib::Request &req, httplib::Response &res) {

            // by default
            res.set_header("AccessControlAllowOrigin", "*");

            // manage a new proposed game
            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);

                std::cout << "------------------------" << std::endl;
                std::cout << "[/game] Received data: " << json_data << std::endl;

            
                if (json_data.contains("game")) {
   
                    int gameId = json_data["game"].get<int>();
                    auto className = json_data["className"].get<std::string>();

                    std::vector<std::shared_ptr<Player>> players;     
                    
                    for (const auto &p : json_data["players"]) {

                        auto role = p["role"].get<Role>();
                        auto name = p["name"].get<std::string>();

                        std::shared_ptr<Player> player;

                        // use of the basic 
                        if (role == Role::HIDER) {
                            player = HiderFactory::create(className, name);
                        } else {
                            player = SeekerFactory::create(className, name);
                        }

                        players.push_back(player);

                        std::cout << "[Game " << gameId << "] new player created: " << *player << std::endl;
                    }

                    // create new game there
                    games.insert(std::make_pair(gameId, players));
                }

                res.status = 200;
                res.set_content("OK", "text/plain"); 
            }
            catch(std::exception &e){
                std::cout << e.what() << std::endl;
                res.status = 406;
                res.set_content("Invalid body content", "text/plain");   
            }
        });

        svr.Post("/game/maze", [this](const httplib::Request &req, httplib::Response &res) {

            // by default
            res.set_header("AccessControlAllowOrigin", "*");

            // manage a new maze generation
            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);

                std::cout << "------------------------" << std::endl;
                std::cout << "[/game/maze] Received data: " << json_data << std::endl;

                // only hider can generate Maze

                if (this->role == Role::HIDER) {
                    
                    if (json_data.contains("game") && json_data.contains("width") && json_data.contains("height")) {
   
                        int gameId = json_data["game"].get<int>();
                        int width = json_data["width"].get<int>();
                        int height = json_data["height"].get<int>();

                        auto maze = MazeFactory::create(this->mazeClassName, width, height);

                        // fill the maze cells
                        maze->build();

                        nlohmann::json output_data;
                        output_data["game"] = gameId;
                        output_data["maze"] = maze->toJSON();
                    
                        std::cout << "[Game " << gameId << "] new maze of size (" << width << "," << height << ") created" << std::endl;

                        res.status = 200;
                        res.set_content(output_data.dump(), "application/json");
                    }
                }
                else{

                    res.status = 406;
                    res.set_content("Client is not a Hider", "application/json"); 
                }
            }
            catch(std::exception &e){
                std::cout << e.what() << std::endl;
                res.status = 406;
                res.set_content("Invalid body content", "text/plain");   
            }
        });

        svr.Post("/game/step", [this](const httplib::Request &req, httplib::Response &res) {

            // by default
            res.set_header("AccessControlAllowOrigin", "*");

            // manage a new proposed game
            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);

                std::cout << "------------------------" << std::endl;
                std::cout << "[/game/step]" << std::endl;

                if (json_data.contains("game")) {
   
                    int gameId = json_data["game"].get<int>();
                    
                    // find the current game (check if exists)
                    auto element = games.find(gameId);

                    if (element != games.end()) {
                        
                        // retrieve current player
                        auto name = json_data["player"]["name"].get<std::string>();
                        int round = json_data["round"].get<int>();
                        int maxRounds = json_data["maxRounds"].get<int>();

                        auto playerIt = std::find_if(element->second.begin(), element->second.end(), [&name](auto &p){
                            return p->getName() == name;
                        });

                        //set location and orientation
                        (*playerIt)->setOrientation(json_data["player"]["orientation"].get<Orientation>());
                        auto x = json_data["player"]["location"]["x"].get<int>();
                        auto y =json_data["player"]["location"]["y"].get<int>();
                        (*playerIt)->setLocation(x,y);

                        //set hider information
                        if ((*playerIt)->getRole() == Role::HIDER){
                            auto currentHider = std::dynamic_pointer_cast<Hider>(*playerIt);
                            currentHider->setCaptured(json_data["player"]["captured"].get<bool>());

                            if (json_data["player"].contains("block")){
                                currentHider->setBlock(Block::fromJSON(json_data["player"]["block"]));
                            }
                        }

                        if (playerIt != element->second.end()) {
                            
                            // retrieve possible actions
                            std::vector<PlayerAction> actions;
                            for (auto &a : json_data["player"]["possibleActions"]) {

                                auto action = PlayerAction::fromJSON(a);
                                actions.push_back(action);
                            }

                            // retrieve observation
                            auto obs = Observation::fromJSON(json_data["player"]["observation"]);

                            // need to get observation
                            PlayerAction chosenAction = (*playerIt)->play(obs, actions);

                            std::cout << " -- [Game: " << gameId << ", round: " << round << "/" << maxRounds << "] " << *(*playerIt) << " wants to play: " << actionToString(chosenAction) << std::endl;

                            nlohmann::json json_action = chosenAction.toJSON();

                            res.status = 200;
                            res.set_content(json_action.dump(), "application/json");
                        }
                        else 
                        {
                            res.status = 406;
                            res.set_content("Player not found", "text/plain"); 
                        }
                    } 
                    else {

                        res.status = 406;
                        res.set_content("Game not found", "text/plain"); 
                    }
                }
                else {

                    res.status = 406;
                    res.set_content("Missing game parameter", "text/plain"); 
                }
            }
            catch(std::exception &e){
                std::cout << e.what() << std::endl;
                res.status = 406;
                res.set_content("Invalid body content", "text/plain");   
            }
        });

        svr.Post("/game/update", [this](const httplib::Request &req, httplib::Response &res) {

            // manage a new IA update
            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);

                std::cout << "------------------------" << std::endl;
                std::cout << "[/game/update]" << std::endl;

                if (json_data.contains("game")) {
                    
                    int gameId = json_data["game"].get<int>();
                    auto previous = Observation::fromJSON(json_data["previous"]);
                    auto current = Observation::fromJSON(json_data["current"]);
                    auto action = PlayerAction::fromJSON(json_data["action"]);

                    auto name = json_data["player"]["name"].get<std::string>();
                
                    // find the current game (check if exists)
                    auto element = games.find(gameId);

                    if (element != games.end()) {

                        auto playerIt = std::find_if(element->second.begin(), element->second.end(), [&name](auto &p){
                            return p->getName() == name;
                        });

                        //set location and orientation
                        (*playerIt)->setOrientation(json_data["player"]["orientation"].get<Orientation>());
                        auto x = json_data["player"]["location"]["x"].get<int>();
                        auto y =json_data["player"]["location"]["y"].get<int>();
                        (*playerIt)->setLocation(x,y);

                        //set hider information
                        if ((*playerIt)->getRole() == Role::HIDER){
                            auto currentHider = std::dynamic_pointer_cast<Hider>(*playerIt);
                            currentHider->setCaptured(json_data["player"]["captured"].get<bool>());

                            if (json_data["player"].contains("block")){
                                currentHider->setBlock(Block::fromJSON(json_data["player"]["block"]));
                            }
                        }

                        if (playerIt != element->second.end()) {
                            
                            std::cout << "Process to the update for player: " << *(*playerIt) << std::endl;
                            (*playerIt)->update(previous, current, action);
                        }
                        else {
                            std::cout << "Player not found" << std::endl;
                            res.status = 406;
                            res.set_content("Player not found", "application/json"); 
                        }
                    } 
                    else {
                        std::cout << "Game not found" << std::endl;
                        res.status = 406;
                        res.set_content("Game not found", "application/json"); 
                    }
                }
                else {
                    std::cout << "Game id not found" << std::endl;
                    res.status = 406;
                    res.set_content("Game id not found", "application/json"); 
                }
            }
            catch(std::exception &e){
                std::cout << e.what() << std::endl;
                res.status = 406;
                res.set_content("Invalid body content", "text/plain");   
            }
        });

        svr.Post("/game/end", [this](const httplib::Request &req, httplib::Response &res) {
             // manage a new proposed game
            try {
                nlohmann::json json_data = nlohmann::json::parse(req.body);

                std::cout << "------------------------" << std::endl;
                std::cout << "[/game/end]" << std::endl;

                if (json_data.contains("id")) {

                    int gameId = json_data["id"].get<int>();
                    
                    // find the current game (check if exists)
                    auto element = games.find(gameId);

                    if (element != games.end()) {
                        
                        // retrieve current winner
                        Role winner = json_data["winner"].get<Role>();
                        
                        std::cout << "Winner of the game " << gameId << " is: " << roleToString(winner) << std::endl;
                        
                        // remove the found game
                        games.erase(element);
                    } 
                    else {
                        std::cout << "Game not found" << std::endl;
                    }
                }
                else {
                    std::cout << "Missing game parameter" << std::endl;
                }
            }
            catch(std::exception &e){
                std::cout << e.what() << std::endl;
            }
        });

        svr.Post("/disconnect", [this](const httplib::Request &req, httplib::Response &res) {

            // manage the server deconnection
            
            std::cout << "------------------------" << std::endl;
            std::cout << "[/disconnect]" << std::endl;

            std::cout << "Server is disconnected... Client is now closed" << std::endl;
            exit(0);
        });
    }


    std::string address;
    int port;
    Role role;
    std::string className;
    std::string mazeClassName;

    // store the Game ID and its associated players
    std::map<int, std::vector<std::shared_ptr<Player>>> games;
    httplib::Server svr;
};

#endif // HIDE_AND_SEEK_REST_CLIENT_HPP