#ifndef HIDE_AND_SEEK_UTILS_POINT_HPP
#define HIDE_AND_SEEK_UTILS_POINT_HPP

#include "utils/json.hpp"

/**
 * @brief Point structure for location into Maze
 * 
 */
struct Point
{
    int x;
    int y;

    Point(int x, int y) : x(x), y(y) {}

    // non expected location into Maze by default
    Point() {
        x = -1;
        y = -1;
    }

    bool operator ==(const Point &p)
    {
        return x == p.x && y == p.y;
    }

    bool operator !=(const Point &p)
    {
        return !(x == p.x && y == p.y);
    }

    nlohmann::json toJSON() const {
        nlohmann::json json_data;

        json_data["x"] = x;
        json_data["y"] = y;

        return json_data;
    }
};

#endif