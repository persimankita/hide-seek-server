#include "helper.hpp"

#include "game.hpp"
#include "utils/graph.hpp"
#include "player/player.hpp"
#include "maze/maze.hpp"
#include "maze/block/material/material.hpp"

bool Helper::isValid(const Game &game) {      

    auto maze = game.getMaze();

    // 1. Check maze construction
    // Count kind of each block
    // Check structure
    unsigned int nBlock = 0;
    unsigned int nWood = 0;
    unsigned int nStone = 0;
    unsigned int nMetal = 0;
    double totalBlocks = maze->getHeight() * maze->getWidth();

    for (unsigned int i = 0; i < maze->getHeight(); i++) {
        for (unsigned int j = 0; j < maze->getWidth(); j++) {
            
            // get current block
            auto block = maze->getCell(j, i)->getBlock();

            // check border limitation
            if (i == 0 || j == 0 || i == (maze->getHeight() - 1) || j == (maze->getWidth() - 1))
                if (block->getKind() != BKind::BORDER)
                    return false;
            
            if (block->getKind() == BKind::MATERIAL) {
                auto material = std::dynamic_pointer_cast<Material>(block);

                if (material->getMaterialKind() == MaterialKind::WOOD)
                    nWood++;
                if (material->getMaterialKind() == MaterialKind::STONE)
                    nStone++;
                if (material->getMaterialKind() == MaterialKind::METAL)
                    nMetal++;
            }

            if (block->getKind() != BKind::GROUND && block->getKind() != BKind::UNDEFINED)
                nBlock++;
        }
    }

    // check quantity of movable and breakable block
    if ((nWood / totalBlocks) > (double)GameParams::pctWood) {

        std::cout << "[Invalid] percent of Wood is: " << nWood / totalBlocks << std::endl;
        return false;
    }

    if ((nStone / totalBlocks) > (double)GameParams::pctStone) {
        std::cout << "[Invalid] percent of Stone is: " << nStone / totalBlocks << std::endl;
        return false;
    }

    if ((nMetal / totalBlocks) > (double)GameParams::pctMetal) {
        std::cout << "[Invalid] percent of Metal is: " << nMetal / totalBlocks << std::endl;
        return false;
    }

    if ((nBlock / totalBlocks) > (double)GameParams::pctBlock) {
        std::cout << "[Invalid] percent of blocks is: " << nBlock / totalBlocks << std::endl;
        return false;
    }
        

    // generate graph from maze
    Graph graph(maze);

    // 2. check if path exists for each seeker to hiders
    for (auto seeker : game.getPlayers(Role::SEEKER)) {
        for (auto hider : game.getPlayers(Role::HIDER)) {
            
            // retrieve current cell of seeker
            Point seekerLoc = seeker->getLocation();
            auto seekerCell = maze->getCell(seekerLoc.x, seekerLoc.y);

            // retrieve current cell of hider
            Point hiderLoc = hider->getLocation();
            auto hiderCell = maze->getCell(hiderLoc.x, hiderLoc.y);

            // check if path exists (from Seeker to Hider)
            bool hasPath = graph.findPath(seekerCell, hiderCell);

            if (!hasPath)
                return false;
        }
    }

    return true;
}