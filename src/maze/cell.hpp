#ifndef HIDE_AND_SEEK_MAZE_CELL_HPP
#define HIDE_AND_SEEK_MAZE_CELL_HPP

#include "maze/block/block.hpp"
#include "player/player.hpp"
// Pre-declaration
class Player;
class Block;

class MazeCell
{
private:
    std::shared_ptr<Block> block;
    std::shared_ptr<Player> player;
    
public:
    MazeCell(std::shared_ptr<Block> block) : block(block), player(nullptr) {};
    MazeCell(std::shared_ptr<Block> block, std::shared_ptr<Player> player) : block(block), player(player) {};
    
    bool hasPlayer();
    const std::shared_ptr<Block> getBlock() const;
    const std::shared_ptr<Player> getPlayer() const;
    void setPlayer(std::shared_ptr<Player> player);
    void setBlock(std::shared_ptr<Block> block);

    nlohmann::json toJSON() const;

    // retrieve only block
    static std::shared_ptr<MazeCell> fromJSON(const nlohmann::json &data);

    ~MazeCell();
};

#endif
