#ifndef HIDE_AND_SEEK_MAZE_BLOCK_HPP
#define HIDE_AND_SEEK_MAZE_BLOCK_HPP

#include <string>
#include <utils//json.hpp>

#include "maze/block/kind.hpp"

constexpr int RESISTANCE_MAX = 100;

/**
 * @brief Main class of block representation of Maze
 * 
 */
class Block
{
protected:

    int resistance;
    bool available;
    bool movable;

public:
    Block() : resistance(RESISTANCE_MAX), available(true), movable(false) {};

    virtual BKind getKind() const = 0;
    virtual void interact() = 0;
    virtual nlohmann::json toJSON() const;
    static std::shared_ptr<Block> fromJSON(const nlohmann::json &data); 

    /**
     * @brief Determine if block is always available in game or not (need to be replaced by ground)
     * 
     * @return true 
     * @return false 
     */
    bool isAvailable() {
        return available;
    };

    bool isMovable() {
        return movable;
    };

    ~Block() {};
};

#endif