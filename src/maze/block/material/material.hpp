#ifndef HIDE_AND_SEEK_BLOCK_MATERIAL_HPP
#define HIDE_AND_SEEK_BLOCK_MATERIAL_HPP

#include <string>
#include <utils/json.hpp>

#include "maze/block/block.hpp"
#include "maze/block/kind.hpp"

enum MaterialKind {
    WOOD,
    STONE,
    METAL
};

class Material : public Block
{
private:
    int hitDamage;

public:
    Material(int hitDamage, bool movable) : Block(), hitDamage(hitDamage) {
        this->movable = movable;
    };

    Material(int resistance, bool available, int hitDamage, bool movable) : Block(), hitDamage(hitDamage) {
        this->resistance = resistance;
        this->available = available;
        this->movable = movable;
    };

    virtual BKind getKind() const {
        return BKind::MATERIAL;
    };

    virtual nlohmann::json toJSON() const;
    static std::shared_ptr<Block> fromJSON(const nlohmann::json &data);

    virtual MaterialKind getMaterialKind() const  = 0;

    virtual void interact() {
        this->resistance -= hitDamage;

        if (this->resistance <= 0) {
            this->available = false;
        }
    };

    ~Material() {};
};

std::string materialKindToString(enum MaterialKind kind);


#endif