#include "maze/block/material/material.hpp"

#include "maze/block/material/custom.hpp"
#include "maze/block/undefined.hpp"


std::string materialKindToString(enum MaterialKind kind) {
    switch (kind)
    {
    case WOOD:
        return "WOOD";
        break;
    case STONE:
        return "STONE";
        break;
    case METAL:
        return "METAL";
        break;       
    default:
        return "UNKNOWN";
        break;
    }
}

nlohmann::json Material::toJSON() const {
    
    // call mother method and add some specification
    nlohmann::json j = Block::toJSON();

    j["hitDamage"] = hitDamage;
    j["material"] = this->getMaterialKind();
    j["materialStr"] = materialKindToString(this->getMaterialKind());
    
    return j;
}

std::shared_ptr<Block> Material::fromJSON(const nlohmann::json &data) {
    
    MaterialKind material = data["material"].get<MaterialKind>();

    auto resistance = data["resistance"].get<int>();
    auto available = data["available"].get<bool>();
    auto movable = data["movable"].get<bool>();
    auto hitDamage = data["hitDamage"].get<int>();
    
    // call mother method and add some specification
    switch (material)
    {
    case WOOD:
        return std::make_shared<Wood>(resistance, available, hitDamage,  movable);
        break;
    case STONE:
        return std::make_shared<Stone>(resistance, available, hitDamage,  movable);
        break;
    case METAL:
        return std::make_shared<Metal>(resistance, available, hitDamage,  movable);
        break;       
    default:
        return std::make_shared<Undefined>();
        break;
    }
}