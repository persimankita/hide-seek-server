#include "maze/observation.hpp"

#include "maze/cell.hpp"
#include "player/player.hpp"
#include "factory/playerFactory.hpp"

/**
 * @brief Display the current block observation
 * 
 * @return std::string 
 */
std::string Observation::toString() const {
    std::string obsStr = "";

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            
            unsigned int cellIndex = (i * this->width) + j;

            if (cells[cellIndex]->getPlayer() != nullptr)
                obsStr += cells[cellIndex]->getPlayer()->getName() + " ";
            else    
                obsStr += blockKindToString(cells[cellIndex]->getBlock()->getKind()) + "  ";
        }

        obsStr += "\n";
    }   

    return obsStr;
}

const nlohmann::json Observation::toJSON() const {

    nlohmann::json json;

    json["height"] = height;
    json["width"] = width;

    nlohmann::json json_cells;

    for (auto c : cells) {

        nlohmann::json current_cell;

        current_cell["block"] = c->getBlock()->toJSON();

        // display player if present
        if (c->hasPlayer())
            current_cell["player"] = c->getPlayer()->toJSON();

        json_cells.push_back(current_cell);
    }

    json["cells"] = json_cells;

    return json;
}

std::shared_ptr<Observation> Observation::fromJSON(const nlohmann::json &data) {

    int height = data["height"].get<int>();
    int width = data["width"].get<int>();

    std::vector<std::shared_ptr<MazeCell>> cells;

    for (auto &c : data["cells"]) {
        
        auto block = Block::fromJSON(c["block"]);
        std::shared_ptr<MazeCell> cell;

        if (c.contains("player")) {

            Role role = c["player"]["role"].get<Role>();

            std::shared_ptr<Player> player;

            if (role == Role::HIDER)
                player = PlayerFactory<Hider>::fromJSON("hider", c["player"]);
            else
                player = PlayerFactory<Seeker>::fromJSON("seeker", c["player"]);

            cell = std::make_shared<MazeCell>(block, player);
        }
        else {
            cell = std::make_shared<MazeCell>(block);
        }

        cells.push_back(cell);
    }

    return std::make_shared<Observation>(width, height, cells);
}