#ifndef HIDE_AND_SEEK_INTERMEDIATE_MAZE_HPP
#define HIDE_AND_SEEK_INTERMEDIATE_MAZE_HPP

#include "maze/maze.hpp"
#include "game.hpp"

/**
 * @brief Example of Maze implementation with 400 blocs
 * 
 */
class IntermediateMaze : public Maze
{

public:
    IntermediateMaze(unsigned int width, unsigned int height) : Maze(GameParams::height,GameParams::width) {};

    virtual void build();

    ~IntermediateMaze() {};
};

#endif
