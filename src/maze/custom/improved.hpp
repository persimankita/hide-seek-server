//
/* created by idriss on 07/06/2022
*/ 
#ifndef IMPROVED_IMPROVED_HPP
#define IMPROVED_IMPROVED_HPP

#include "maze/maze.hpp"
#include "game.hpp"

/**
 * @brief Example of Maze implementation with 400 blocs
 * 
 */
class ImprovedMaze : public Maze
{

public:
    ImprovedMaze(unsigned int width, unsigned int height) : Maze(GameParams::height,GameParams::width) {};

    virtual void build();

    ~ImprovedMaze() {};
};

#endif