#include "maze/custom/intermediate.hpp"

#include "maze/block/block.hpp"


// specific blocks
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/material/custom.hpp"
#include "utils/randomGenerator.hpp"
#include "maze/block/kind.hpp"
#include "maze/block/material/material.hpp"
#include "random"
void IntermediateMaze::build(){
      // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    std::array<std::shared_ptr<Block>,GameParams::width*GameParams::height> maze_matrix;
    float pct_block = GameParams::pctBlock;
    float pct_wood = GameParams::pctWood;
    float pct_stone = GameParams::pctStone;
    float pct_metal = GameParams::pctMetal;
    // 76 is the number of border blocs already installed witch is the perimeter of the maze
    unsigned int nbre_block = pct_block*(height*width)-76; 
    unsigned int nbre_wood = pct_wood*(height*width);
    unsigned int nbre_stone = pct_stone*(height*width);
    unsigned int nbre_metal = pct_metal*(height*width);
    unsigned int compteur = 0;

    // build the borders for current maze
    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            //std::cout<<i*width+j<<std::endl;
            
                if (i == 0 || j == 0 || i == (this->height - 1) || j == (this->width - 1)) {
                maze_matrix[i*width+j] = std::make_shared<Border>();
                }
               else {
                   maze_matrix[i*width+j] = std::make_shared<Ground>();
                }
        }
    } 
    // fill the maze with wood
    for (unsigned int i = 1 ; i<(height-1); i++){
        for (unsigned int j = 1 ; j<(width-1); j++){
            while(compteur<nbre_wood){
                int index = rGenerator->getRandomInt(324);
                if(maze_matrix[index]->getKind() == BKind::GROUND){
                    maze_matrix[index] = std::make_shared<Wood>();
                    compteur++;
                }
            }
        }
    }
    compteur=0;
    // fill the maze with Stone
        for (unsigned int i = 1 ; i<(height-1); i++){
        for (unsigned int j = 1 ; j<(width-1); j++){
            while(compteur<nbre_stone){
                int index = rGenerator->getRandomInt(324);
                if(maze_matrix[index]->getKind() == BKind::GROUND){
                    maze_matrix[index] = std::make_shared<Stone>();
                    compteur++;
                }
            }
        }
    }
    compteur=0;
    // fill the maze with Metal
        for (unsigned int i = 1 ; i<(height-1); i++){
        for (unsigned int j = 1 ; j<(width-1); j++){
            while(compteur<nbre_metal){
                int index = rGenerator->getRandomInt(324);
                if(maze_matrix[index]->getKind() == BKind::GROUND){
                    maze_matrix[index] = std::make_shared<Metal>();
                    compteur++;
                }
            }
        }
    }
    compteur=0;
    // fill the maze with walls 
    for (unsigned int i = 1 ; i<(height-1); i++){
        for (unsigned int j = 1 ; j<(width-1); j++){
     while(compteur<nbre_block-(nbre_metal+nbre_stone+nbre_wood)){
                int index = rGenerator->getRandomInt(324);
                if(maze_matrix[index]->getKind() == BKind::GROUND){
                    maze_matrix[index] = std::make_shared<Border>();
                    compteur++;
                }
                
            }
        }
    }

 /*  for (unsigned int i =1; i<(this->height-1) ;i++){
        for (unsigned int j = 1;j>(this->width-1) ;j++ ){
            if(rGenerator->getProbability()<0.4){
            // add wood blocs
           // while(compteur<nbre_wood){
                if(maze_matrix[i*(width-1)+j]->getKind()== BKind::GROUND){
                    //std::cout<<rGenerator->getRandomInt(324)<<std::endl;
                    maze_matrix[i*(width-1)+j] = std::make_shared<Wood>();
                    //std::cout<< i*(width-1)+j<<std::endl;
                    compteur++;
                    //std::cout<< i*(width-1)+j<<std::endl;
                }
           // }
        }
            }
            //std::cout<<compteur<<std::endl;
        
    }    */    
 
  
     
     
     //construct the builed maze 
    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            std::shared_ptr<MazeCell> cell = std::make_shared<MazeCell>(maze_matrix[i*width+j]);
            cells.push_back(cell);
            
        }
    }
   
    
     
     
     
     
     
     
     
      /* // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

        // step 1 create labyrinthe parfait

        // I am using array because we already know the size (400) also for quick access to element 
    std::array<std::shared_ptr<Block>, 400> inter_laby;
        // step 2 modify the percentage of blocks to be as known (160bloc)
        // step 3 randomly fill the maze with the exact percentage of component */
}