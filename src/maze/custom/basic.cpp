#include "maze/custom/basic.hpp"

#include "maze/block/block.hpp"

// specific blocks
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/material/custom.hpp"
#include "utils/randomGenerator.hpp"

/**
 * @brief Basic random maze
 * 
 * Warning: this maze generator is randomly done. It's possible the generated maze is not valid.
 */
void BasicMaze::build() {

    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
    
    // build the current Maze
    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            
            std::shared_ptr<Block> block;

            // add border or ground
            if (i == 0 || j == 0 || i == (this->height - 1) || j == (this->width - 1)) {
                block = std::make_shared<Border>();
            } else {
                
                // add randomly Wood block
                if (rGenerator->getProbability() < 0.10) {
                    
                    double p = rGenerator->getProbability();

                    // different kind of materials
                    if (p < 0.6) {
                        block = std::make_shared<Wood>();
                    }
                    else if (p < 0.9) {
                        block = std::make_shared<Stone>();
                    } else {
                        block = std::make_shared<Metal>();
                    }
                }
                else{
                    block = std::make_shared<Ground>();
                }
            }

            // construct new shared ptr Cell with only block
            std::shared_ptr<MazeCell> cell = std::make_shared<MazeCell>(block);
            cells.push_back(cell);
        }
    }
}
