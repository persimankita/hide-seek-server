#ifndef HIDE_AND_SEEK_MAZE_OBSERVATION_HPP
#define HIDE_AND_SEEK_MAZE_OBSERVATION_HPP

#include <tuple>
#include <memory>
#include <utils/json.hpp>

// Pre-declaration
class MazeCell;

/**
 * @brief Reduce information sent to Player into order to play its next move
 * 
 */
class Observation
{

private:
    unsigned int width;
    unsigned int height;

    std::vector<std::shared_ptr<MazeCell>> cells;

public:
    Observation(unsigned int width, unsigned int height, std::vector<std::shared_ptr<MazeCell>> cells) 
        : width(width), height(height), cells(cells) {};

    const std::tuple<int, int> getSize() const {
        return std::make_pair(width, height);
    }

    const std::vector<std::shared_ptr<MazeCell>> &getCells() const {
        return cells;
    }

    std::string toString() const;

    const nlohmann::json toJSON() const;

    static std::shared_ptr<Observation> fromJSON(const nlohmann::json &data);

    friend std::ostream &operator<<(std::ostream &os, const Observation &obs) { 
        return os << obs.toString();
    }

    ~Observation() {};
};


#endif
