#include "game.hpp"
#include "factory/playerFactory.hpp"
#include "factory/mazeFactory.hpp"

// specific incluces
#include "maze/custom/intermediate.hpp"
#include "player/seeker/random.hpp"
#include "player/hider/random.hpp"
#include "player/seeker/amateur.hpp"
#include "player/hider/amateur.hpp"
// test game validity
#include "utils/helper.hpp"

// default params
namespace Params {

    std::string hiderClassName = "amateur";
    std::string seekerClassName = "amateur";
    std::string mazeClassName = "intermediate"; // by default


    int nGames = 100;
};

int main(int argc, char* argv[]) {

    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<IntermediateMaze>("intermediate");
    //Amateur Bots
    SeekerFactory::registerClass<AmateurSeeker>("amateur");
    HiderFactory::registerClass<AmateurHider>("amateur");

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {
            
            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {
            
            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: " 
                                        << "{hider: " << Params::hiderClassName
                                        << ", seeker: " << Params::seekerClassName
                                        << ", maze: " << Params::mazeClassName
                                        << "}" << std::endl;

        // before running
        if (!Helper::isValid(game)) {
            std::cout << "Game is not valid: unexpected maze built..." << std::endl;
            exit(0);
        }

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            std::cout<<"Action choisie :"<<actionToString(chosenAction)<<std::endl;
            cout<<"::-------------------------------------------::"<<endl;
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // updtae the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in " 
                << game.getNumberOfRounds() << " rounds" << std::endl;

        // update stat
        results[winner] += 1;
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for (auto &k : results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins (" 
                << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
    }
}